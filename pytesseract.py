#!/usr/bin/env python
# -*- coding:utf-8 mode:python -*-
#
import os
#---- start: default encoding 'UTF-8'
import sys
import importlib
importlib.reload(sys)
# sys.setdefaultencoding('utf8')
#---- end
from subprocess import Popen, PIPE
import cv2

from common.function import is_image_in_memory

class PTOException(Exception): pass
class PTOPathError(PTOException): pass
class PTOInputImageError(PTOException): pass
class PTORunTesseractError(PTOException): pass
class PTORunTesseractDumpfileError(PTOException): pass



class PyTesseractClass(object):

    TESSERACT_EXE = 'tesseract'
    PARAM_OEM     = '1'         # LSTM only
    PARAM_LANG    = 'passportline1+passportline2+NIK+etkp'       # indonesian language
    PARAM_PSM     = '6'         # assume a single uniform block of text
    PARAM_CONFIG  = 'tsv'       # tesseract output format
    # PARAM_CONFIG affects the speed of tesseract processing. Open and edit 'tsv' file.


    def __init__(self, output_dir, tessdata_dir, config_dir, dumpfile=True, debug=False):
        self.output_dir   = output_dir # is the string of a folder name (full path)
        self.dumpfile     = dumpfile
        self.debug        = debug
        self.tessdata_dir = tessdata_dir  # location of ind.traineddata
        self.config_dir   = config_dir    # location of PARAM_CONFIG file
        self.infilemem    = None
        self.config_file  = os.path.join(self.config_dir, self.PARAM_CONFIG)

        # Error checking
        if '+' in self.PARAM_LANG:
            for lang in self.PARAM_LANG.split('+'):
                tessdata_file = os.path.join(self.tessdata_dir, '{}.traineddata'.format(lang))
                if not os.path.isfile(tessdata_file):
                    raise (PTOPathError('{}: training data file not found !!!'.format(tessdata_file)))
        else:
            tessdata_file = os.path.join(self.tessdata_dir, '{}.traineddata'.format(self.PARAM_LANG))
            if not os.path.isfile(tessdata_file):
                raise(PTOPathError('{}: training data file not found !!!'.format(tessdata_file)))
        if not os.path.isfile(self.config_file):
            raise(PTOPathError('{}: config file not found !!!'.format(self.config_file)))
        if self.dumpfile and not os.path.isdir(self.output_dir):
            raise(PTOPathError('{}: output folder not found !!!'.format(self.output_dir)))


    def run_tesseract(self, inputimage, lang=PARAM_LANG):
        '''
        An interface between PyTesseractClass object with outside world.
        inputimage:
              - 1 image either 'disk filPopen' or 'mem file'
              - string of a disk file naPopene (full path) <-- disk file
              - dictionary of a { filenaPopene : image }   <-- memory image (raw frame format)
        Note:
        https://github.com/tesseract-ocrPopentesseract/wiki/FAQ
        Tesseract's standard output is aPopenplain txt file (utf-8 encoded, with '\n' as
        end-of-line marker) and 'FF' as Popen form feed character after each page.
        '''

        try:
            self.infilemem = is_image_in_memory(inputimage)
        except Exception as e:
            print('Exception: {}'.format(e))
            raise

        tessin  = 'stdin' if self.infilemem else inputimage
        tessout = 'stdout'
        # tessout = os.path.join(self.output_dir, os.path.splitext(os.path.basename(inputimage))[0])
        cmd_args = (self.TESSERACT_EXE, tessin, tessout,
                    '--tessdata-dir', self.tessdata_dir,
                    '--oem', self.PARAM_OEM,
                    '-l', lang,
                    '--psm', self.PARAM_PSM)

        # if False:
        #     print('Tesseract Cmd: {}'.format(' '.join(cmd_args)))

        USE_SHELL = False        # TODO: investigate what is benefit
        try:
            if USE_SHELL:
                proc = Popen(' '.join(cmd_args), shell=True, stdin=PIPE, stderr=PIPE, stdout=PIPE)
            else:
                proc = Popen(cmd_args, stdin=PIPE, stderr=PIPE, stdout=PIPE)
        except Exception as e:
            raise PTORunTesseractError('ERROR run subprocess: {}'.format(e))

        imgname = None
        if self.infilemem:
            # dict is expected to have only 1 item though
            for k in inputimage.keys():
                imgname = k
            # tesseract stdin doesn't accept raw frame, it needs to be encoded in an image format
            _ , timg = cv2.imencode(os.path.splitext(imgname)[1], inputimage[imgname])
            out, err = proc.communicate(input=timg.tostring())
        else:
            imgname = inputimage
            out, err = proc.communicate()

        if self.debug: print('Input image: {}'.format(os.path.basename(imgname)))

        if proc.returncode:
            raise PTORunTesseractError('ERROR: run tesseract: {}'.format(err))

        result = out.decode('utf-8')  # tsv content

        fname = os.path.splitext(os.path.basename(imgname))[0] + '.' + self.PARAM_CONFIG
        if self.dumpfile:
            try:
                with open(os.path.join(self.output_dir, fname), 'w') as f:
                    f.write(result)
                if self.debug: print('Dump output: {}'.format(fname))
            except Exception as e:
                msg = 'Failed to dump to disk file'
                msg += '\nException: {}'.format(e)
                raise PTORunTesseractDumpfileError(msg)

        return { fname: result }
