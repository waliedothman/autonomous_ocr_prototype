#!/usr/bin/env python
# -*- coding:utf-8 mode:python -*-
#
import os
import shutil
import sys
import time
import math
import numpy as np
import cv2
from pathlib import Path
import multiprocessing
import tensorflow as tf
from tensorflow.python.client import timeline

from common.function import save_box_coordinate
from common.config import NUM_PROCESSORS, root_dir
sys.path.append(root_dir)
from textdetect.nets import model_train as model
from textdetect.utils.rpn_msr.proposal_layer import proposal_layer
from textdetect.utils.text_connector.detectors import TextDetector

tf.app.flags.DEFINE_string('gpu', '0', '')
tf.app.flags.DEFINE_string('checkpoint_path', 'checkpoints_mlt/', '')



class TextdetectClass(object):
    '''
    Detect box coordinates ...
    '''
    def __init__(self,
                 detect_dir,
                 model_path = None,
                 dumpfile=True,
                 debug=False,
                 gunicorn = False,
                 show_individual_box = False,
                 gpu_memory_fraction = 1.0,
                 allow_growth = True ):
        self.detect_dir          = detect_dir
        self.dumpfile            = dumpfile
        self.debug               = debug
        self.gunicorn            = gunicorn
        self.show_individual_box = show_individual_box
        self.gpu_memory_fraction = gpu_memory_fraction
        self.allow_growth        = allow_growth

        # init session       
        FLAGS = tf.app.flags.FLAGS        
        if not self.gunicorn:
            ### TODO: Read gunicorn env using setting file
            os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu

        with tf.get_default_graph().as_default():
            self.input_image = tf.placeholder(tf.float32, shape=[None, None, None, 3], name='input_image')
            self.input_im_info = tf.placeholder(tf.float32, shape=[None, 3], name='input_im_info')

            current_scope = tf.get_variable_scope()
            with tf.variable_scope( current_scope, reuse=tf.AUTO_REUSE ):
                gpu_options = tf.GPUOptions( per_process_gpu_memory_fraction = self.gpu_memory_fraction, allow_growth = self.allow_growth )
                global_step = tf.get_variable('global_step', [], initializer=tf.constant_initializer(0), trainable=False)
                self.bbox_pred, self.cls_pred, self.cls_prob = model.model(self.input_image)

                variable_averages = tf.train.ExponentialMovingAverage(0.997, global_step)
                saver = tf.train.Saver(variable_averages.variables_to_restore())
                self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, gpu_options=gpu_options))

                ## These path finding the check points using tf need tuning for web app (i.e. flask, gunicorn )
                ## As such, by fixing the model path, textdetect can only be run in docker.
                # ckpt_state = tf.train.get_checkpoint_state(FLAGS.checkpoint_path)
                # model_path = os.path.join(FLAGS.checkpoint_path, os.path.basename(ckpt_state.model_checkpoint_path))
                try:
                    saver.restore(self.sess, model_path)
                    if self.debug:
                        print('Restoring model parameters from: {}'.format(model_path))
                except Exception as e:
                    print('::EXCEPTION: {}'.format(e))
                    sys.exit(1)

        config = tf.ConfigProto(allow_soft_placement=True, gpu_options=gpu_options)

        # if self.gunicorn:
        #     cpu_cores = 2 
        # else:
        #     cpu_cores = 4 #self.get_cpu_quota_within_docker() or multiprocessing.cpu_count()

        cpu_cores = NUM_PROCESSORS

        config.intra_op_parallelism_threads = cpu_cores
        config.inter_op_parallelism_threads = cpu_cores

        if self.debug:
            print( "CPU_CORES:", cpu_cores )


    def get_cpu_quota_within_docker( self ):
        cpu_cores = None
        cfs_period = Path("/sys/fs/cgroup/cpu/cpu.cfs_period_us")
        cfs_quota = Path("/sys/fs/cgroup/cpu/cpu.cfs_quota_us")

        if cfs_period.exists() and cfs_quota.exists():
            # we are in linux container with cpu quotas!
            with cfs_period.open('rb') as p, cfs_quota.open('rb') as q:
                p, q = int( p.read()), int(q.read())

                # get the cores allocated by dividing the quota
                # in microseconds by the period in microseconds
                cpu_cores = math.ceil( q / p ) if q > 0 and p > 0 else None

        return cpu_cores
  

    def resize_image(self, img):
        img_size = img.shape
        im_size_min = np.min(img_size[0:2])
        im_size_max = np.max(img_size[0:2])

        im_scale = float(600) / float(im_size_min)

        if np.round(im_scale * im_size_max) > 1200:
            im_scale = float(1200) / float(im_size_max)

        new_h = int(img_size[0] * im_scale)
        new_w = int(img_size[1] * im_scale)

        new_h = new_h if new_h // 16 == 0 else (new_h // 16 + 1) * 16
        new_w = new_w if new_w // 16 == 0 else (new_w // 16 + 1) * 16

        re_im = cv2.resize(img, (new_w, new_h), interpolation=cv2.INTER_LINEAR)

        return re_im, (float(new_h) / img_size[0], float(new_w) / img_size[1])


    def detect_box(self, imagename, im, detect_mode='O'):              
        # FLAGS = tf.app.flags.FLAGS        
        # os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu

        new_boxes = []

        # with tf.get_default_graph().as_default():
        #     input_image = tf.placeholder(tf.float32, shape=[None, None, None, 3], name='input_image')
        #     input_im_info = tf.placeholder(tf.float32, shape=[None, 3], name='input_im_info')

        #     # global_step = tf.get_variable('global_step', [], initializer=tf.constant_initializer(0), trainable=False)

        #     bbox_pred, cls_pred, cls_prob = model.model(input_image)

        #     variable_averages = tf.train.ExponentialMovingAverage(0.997, self.global_step)
        #     saver = tf.train.Saver(variable_averages.variables_to_restore())

        #     with tf.Session(config=tf.ConfigProto(allow_soft_placement=True)) as sess:
        #         ckpt_state = tf.train.get_checkpoint_state(FLAGS.checkpoint_path)
        #         model_path = os.path.join(FLAGS.checkpoint_path, os.path.basename(ckpt_state.model_checkpoint_path))
        #         print('Restoring model parameters from: {}'.format(model_path))
        #         saver.restore(sess, model_path)
        if self.debug:
            print('Detecting boxes for image: {}'.format(imagename))
        
        
        img, (rh, rw) = self.resize_image(im)
        h, w, c = img.shape
        im_info = np.array([h, w, c]).reshape([1, 3])

        if self.debug:
            options = tf.RunOptions()
            options.output_partition_graphs = True
            options.trace_level = tf.RunOptions.FULL_TRACE
            metadata = tf.RunMetadata()

            bbox_pred_val, cls_prob_val = self.sess.run( [self.bbox_pred, self.cls_prob], feed_dict={self.input_image: [img], self.input_im_info: im_info}, options = options, run_metadata=metadata )

            fetched_timeline = timeline.Timeline( metadata.step_stats)
            chrome_trace = fetched_timeline.generate_chrome_trace_format()
            # with open( 'timeline_inference_profile.json', 'w') as f:
            #     f.write( chrome_trace )

            # print( metadata.partition_graphs)
            # print( metadata.step_stats)

        else:
            bbox_pred_val, cls_prob_val = self.sess.run( [self.bbox_pred, self.cls_prob], feed_dict={self.input_image: [img], self.input_im_info: im_info} )

        textsegs, _ = proposal_layer(cls_prob_val, bbox_pred_val, im_info)
        scores = textsegs[:, 0]
        textsegs = textsegs[:, 1:5]

        textdetector = TextDetector(DETECT_MODE=detect_mode)
        boxes = textdetector.detect(textsegs, scores[:, np.newaxis], img.shape[:2], im, rh, rw, self.show_individual_box)
        boxes = np.array(boxes, dtype=np.int)

        for box in boxes:
            box_length = len(box)            
            new_box = [ box[i]/rh if i % 2 == 0 else box[i]/rw for i in range(box_length) ]
            new_boxes.append( new_box )

        if self.dumpfile:
            save_box_coordinate(self.detect_dir, imagename, new_boxes)

        return new_boxes
