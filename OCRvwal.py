from PIL import Image, ImageDraw
from detect_box import TextdetectClass
import cv2
import numpy as np
import shapely.geometry as sg
from pytesseract import PyTesseractClass
from os import listdir
from os.path import isfile, join
import string
import jellyfish as jf
from denoisingbg import DenoisingBg

DEBUG = False
DEBUG_PATH = './'
SEGMENTS = 25
PASSPORT_SEGMENTS = 25
ID_SEGMENTS = 8
POLYGON_OVERLAP_THRESHOLD = 0.5
SPREAD = 50
INTERCEPT = 0

def list2pointslist(l):
    return np.array([[l[2*i], l[2*i+1]] for i in range(len(l)//2)])
def sort_points_clockwise(points):
    # sort the points so you get a clockwise ordering
    # code from Rongchao Ma
    coord_sum = points.sum(axis = 1)
    diff = np.diff(points, axis = 1)
    rect = np.zeros((4, 2), dtype = "float32")
    rect[0] = points[np.argmin(coord_sum)]
    rect[2] = points[np.argmax(coord_sum)]
    rect[1] = points[np.argmin(diff)]
    rect[3] = points[np.argmax(diff)]
    return rect
# compute crop size so that the bounding box is contained
def compute_crop_size(points, height, width):
    fpl = []
    left, lower, upper, right = width, 0, height, 0
    for i in range(len(points) // 2):
        left = min(left, points[2*i])
        lower = max(lower, points[2*i+1])
        upper = min(upper, points[2*i+1])
        right = max(right, points[2*i])
        fpl.append([points[2*i], points[2*i+1]])
    # make sure the crop values don't exceed the image
    left = max(left, 0)
    right = min(right, width)
    upper = max(upper, 0)
    lower = min(lower, height)
    return left, lower, upper, right, np.array(fpl)
# compute the angle between a vector from p1 to p2 and the x-axis
def compute_angle(p1,p2):
    vector = p2 - p1
    newwidth = np.linalg.norm(vector)
    angle = -np.arccos(np.dot(vector, [1, 0])/newwidth)
    if vector[1]>0:
        angle = -angle
    angle_degrees = np.degrees(angle)
    return newwidth, angle_degrees
# compute the angle the bounding box makes wrt to the x-axis    
def compute_bb_angle(bb):
    # sort the points clockwise
    (_, _, bottom_right, bottom_left) = sort_points_clockwise(bb)
    # computer the angle the bounding box has
    _, angle_degrees = compute_angle(bottom_left, bottom_right)
    return angle_degrees
# perform a rotation of a point using a rotation_matrix around a centre translation_vector
def rotate_with_matrix_centre(point, rotation_matrix, translation_vector):
    translated = point - translation_vector
    rotated = rotation_matrix.dot(translated)
    return rotated + translation_vector
# rotate a whole bounding box around a translation_vector centre using rotation_matrix
def rotate_bb_with_matrix_centre(bb, rotation_matrix, translation_vector):
    # return rotation_matrix.dot(bb - translation_vector) + translation_vector
    for i in range(bb.shape[0]):
        bb[i] = rotation_matrix.dot(bb[i] - translation_vector) + translation_vector
    return bb
def crop_and_rotate_opencv(img, bb, filename, debug_tag='', segments=SEGMENTS, preset_angle=None, denoise=None):
    # computer the crop size
    left, lower, upper, right, fp = compute_crop_size(bb, img.shape[0], img.shape[1])
    # sort the points clockwise
    (top_left, top_right, bottom_right, bottom_left) = sort_points_clockwise(fp)
    # computer the angle the bounding box has
    newwidth, angle_degrees = compute_angle(bottom_left, bottom_right)
    if preset_angle is not None:    # precomputed angle provided, use that to rotate, adjust bounding box appropriately
        bb_angle = - np.radians(angle_degrees - preset_angle)
        c, s = np.cos(bb_angle), np.sin(bb_angle)
        rotation_matrix = np.array(((c, -s), (s, c)))
        bb_centre = ((top_left + top_right)/2 + (bottom_left + bottom_right)/2)/2
        fp = rotate_bb_with_matrix_centre(fp, rotation_matrix, bb_centre)
        # for i in range(fp.shape[0]):
        #     p = fp[i]
        #     fp[i] = rotate_with_matrix_centre(p, rotation_matrix, bb_centre)
        # repeat crop and sorting because new bounding box was made
        left, lower, upper, right = img.shape[1], 0, img.shape[0], 0
        left, lower, upper, right, fp = compute_crop_size(fp.flatten(), img.shape[0], img.shape[1])
        (top_left, top_right, bottom_right, bottom_left) = sort_points_clockwise(fp)
        # if a preset_angle was given, this will become our angle
        angle_degrees = preset_angle
    # height and width of the rotated bounding box
    newheighth = np.floor(np.linalg.norm(top_left-bottom_left))
    newwidth = np.floor(newwidth)
    # crop the image and turn it to grayscale
    cropped = img[int(np.floor(upper)):int(np.ceil(lower)),int(np.floor(left)):int(np.ceil(right))].copy()

    # cropped = denoisingbg.run_denoising({'img': cropped})['img_mask']
    
    # if debug_tag == 'test':
    #     cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- 0 {debug_tag}- cropped.jpg'), cropped)
    cropped = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
    if DEBUG:
        cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- 0 {debug_tag}- cropped.jpg'), cropped)
    rows,cols = cropped.shape[:2]
    # rotate the image
    M = cv2.getRotationMatrix2D((cols/2,rows/2),angle_degrees,1)
    rotated = cv2.warpAffine(cropped,M,(cols,rows)).copy()
    if DEBUG:
        cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- 1 {debug_tag}- rotated.jpg'), rotated)
    # rotating introduced artifacts since rotation brought in pixels from outside the image
    # we whiten those pixels
    halfw, halfh = (cols - newwidth) // 2 + 1, (rows - newheighth) // 2 + 1
    for i in range(cols):
        for j in range(rows):
            if halfh<j<(rows - halfh) and halfw<i<(cols - halfw):  # for efficiency, this should not be looped over
                continue
            else:
                rotated[j,i] = 255
    if denoise is None:
        # if True or DEBUG:
        #     cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- 1 {debug_tag}- rotated_whitened.jpg'), rotated)
        return angle_degrees, rotated
    # denoising: we turn the image into black and white    
    blackAndWhiteImage = np.ones((rows,cols), np.uint8)*255
    # if no number of segments are provided, we try to make them about 66 pixels wide
    # if segments is None:
    #     segments = max(1, cols // 100)
    #     # if cols > 66:
    #     #     rest = cols % 66
    #     #     if rest > 15:
    #     #         segments += 1
    # # for each segment, calculate mean pixel, and standard deviation, and set a B&W threshold of (mean - std)
    # step = cols / segments
    # for i in range(segments):
    #     lb, ub = int(i*step), int((i+1)*step)
    #     segment = cropped[0:rows, lb:ub]
    #     mean = cv2.mean(segment)[0]
    #     std = np.sqrt(np.var(segment))
    #     # if std < 10:
    #     #     
    #     # print(f'mean: {mean:.2f} and std: {std:.2f}')
    #     # f = (std - 10)/50
    #     # f = (std - 5)/50
    #     f = (std - INTERCEPT) / SPREAD
    #     (_, blackAndWhiteImageSlice) = cv2.threshold(rotated[0:rows, lb:ub], max(0 , mean - f*std), 255, cv2.THRESH_BINARY)
    #     blackAndWhiteImage[0:rows, lb:ub] = blackAndWhiteImageSlice
    segment_width = 80
    lb, ub = 0, segment_width
    while ub < cols:
        segment = cropped[0:rows, lb:ub]
        mean = cv2.mean(segment)[0]
        std = np.sqrt(np.var(segment))
        # print(f'mean: {mean:.2f} and std: {std:.2f}')
        f = (std - INTERCEPT) / SPREAD
        (_, blackAndWhiteImageSlice) = cv2.threshold(rotated[0:rows, lb:ub], max(0 , mean - f*std), 255, cv2.THRESH_BINARY)
        blackAndWhiteImage[0:rows, lb:ub] = blackAndWhiteImageSlice
        lb += segment_width
        ub += segment_width
    if lb < segment_width:
        ub, lb = cols, max(0, cols - 100)
        segment = cropped[0:rows, lb:ub]
        mean = cv2.mean(segment)[0]
        std = np.sqrt(np.var(segment))
        # print(f'mean: {mean:.2f} and std: {std:.2f}')
        f = (std - INTERCEPT) / SPREAD
        (_, blackAndWhiteImageSlice) = cv2.threshold(rotated[0:rows, lb:ub], max(0 , mean - f*std), 255, cv2.THRESH_BINARY)
        blackAndWhiteImage[0:rows, lb:ub] = blackAndWhiteImageSlice
    # if debug_tag == 'test':
    #     cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- 3 {debug_tag}- black_and_white.jpg'), blackAndWhiteImage)
    if DEBUG:
        cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- 3 {debug_tag}- black_and_white.jpg'), blackAndWhiteImage)
    return angle_degrees, blackAndWhiteImage

def do_ocr_passport(filename, path):
    print(f'\n{filename}')
    imagepath = join(path, filename)
    imagedata = cv2.cvtColor(np.array(Image.open(imagepath).convert('RGB')), cv2.COLOR_RGB2BGR)
    [h,w] = imagedata.shape[:2]
    if w < 2000:
        r = 2000/w
        d = (int(w*r), int(h*r))
        imagedata = cv2.resize(imagedata, d)
    bbs = textdetect.detect_box(imagepath, imagedata)
    bbs.sort(key=lambda x: x[1], reverse=True)
    _, img = crop_and_rotate_opencv(imagedata, bbs[1], filename, debug_tag = 'line1', segments=PASSPORT_SEGMENTS, denoise=True)
    result = tesseract.run_tesseract({imagepath: img}, lang='passportline1')
    key = f'{filename[:-4]}.tsv'
    ocr_str1 = result[key][:-2]
    print(7*' ' + 44*'-')
    print(f'line1: {ocr_str1}')
    _, img = crop_and_rotate_opencv(imagedata, bbs[0], filename, debug_tag = 'line2', segments=PASSPORT_SEGMENTS, denoise=True)
    result = tesseract.run_tesseract({imagepath: img}, lang='passportline2')
    key = f'{filename[:-4]}.tsv'
    ocr_str2 = result[key][:-2]
    print(f'line2: {ocr_str2}')
    print(7*' ' + 44*'-')
    if DEBUG:
        font = cv2.FONT_HERSHEY_PLAIN
        imagedata = cv2.putText(imagedata, ocr_str1, (int(bbs[1][0]), int(bbs[1][1])), font, 4, (0,0,255), 2)
        imagedata = cv2.putText(imagedata, ocr_str2, (int(bbs[0][0]), int(bbs[0][1])), font, 4, (0,0,255), 2)
        cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- ocr.jpg'), imagedata)

def do_ocr_id(filename, path, detect_mode='O'):
    def group_bounding_boxes(bbs, discard, detect_mode='O'):
        if len(bbs) == 0:
            return None, []
        elif len(bbs) == 1:
            return None, [[0]]
        # compute the angle of the 2nd top bounding box
        # probably most accurate because of all capital letters
        if detect_mode == 'O':
            angle = compute_bb_angle(list2pointslist(bbs[1]))
            bb_angle = - np.radians(angle)
            c, s = np.cos(bb_angle), np.sin(bb_angle)
            rotation_matrix = np.array(((c, -s), (s, c)))
        else:
            angle = 0
        rotated_bbs = []
        for i in range(len(bbs)):
            bb = list2pointslist(bbs[i])
            bb_centre = ((bb[0] + bb[1])/2 + (bb[2] + bb[3])/2)/2
            if detect_mode == 'O':
                rotated_bb = rotate_bb_with_matrix_centre(bb, rotation_matrix, bb_centre)
            else:
                rotated_bb = bb
            rotated_bbs.append({'tag': i, 'bounding_box': rotated_bb})
        rotated_bbs.sort(key=lambda x: x['bounding_box'][0][1])
        # move bounding boxes on top of each other by x-axis translations so their centres match
        # check if they overlap more than POLYGON_OVERLAP_THRESHOLD, then they're on the same line
        def check_overlap(bb1, bb2):
            bb1_centre = ((bb1[0] + bb1[1])/2 + (bb1[2] + bb1[3])/2)/2
            bb2_centre = ((bb2[0] + bb2[1])/2 + (bb2[2] + bb2[3])/2)/2
            bb2_translated = bb2 - np.array([bb2_centre[0] - bb1_centre[0], 0])
            bb1_polygon = sg.Polygon(bb1)
            bb2_polygon = sg.Polygon(bb2_translated)
            bb_intersection = bb1_polygon.intersection(bb2_polygon)
            bb1_area, intersection_area = bb1_polygon.area, bb_intersection.area
            if intersection_area/bb1_area > POLYGON_OVERLAP_THRESHOLD:
                return True
            else:
                bb2_area = bb2_polygon.area
                if intersection_area/bb2_area > POLYGON_OVERLAP_THRESHOLD:
                    return True
                else:
                    return False
        # group bounding boxes that are on the same line
        last = rotated_bbs[0]
        grouped = [[last['tag']]]
        for i in range(1, len(rotated_bbs)):
            bb = rotated_bbs[i]
            if bb['bounding_box'][0][0]>discard:
                continue
            if check_overlap(last['bounding_box'], bb['bounding_box']):
                grouped[-1].append(bb['tag'])
            else:
                grouped.append([bb['tag']])
            last = bb
        def sort_key(x):
            bb = None
            for b in rotated_bbs:
                if b['tag'] == x:
                    bb = b['bounding_box']
                    return (((bb[0] + bb[1])/2 + (bb[2] + bb[3])/2)/2)[0]
        for group in grouped:
            group.sort(key=sort_key )
        return angle, grouped
        
    print(f'\n{filename}')
    imagepath = join(path, filename)
    imagedata = cv2.cvtColor(np.array(Image.open(imagepath).convert('RGB')), cv2.COLOR_RGB2BGR)
    if DEBUG:
        cv2.imwrite(join(DEBUG_PATH, filename), imagedata)
    # imagedata = remove_color(imagedata, filename)

    # imagedata = remove_color_bis(imagedata, filename)
    # [h,w] = imagedata.shape[:2]
    # if w > 600:
    #     r = 400/w
    #     d = (int(w*r), int(h*r))
    #     imagedata = cv2.resize(imagedata, d)
    [h,w] = imagedata.shape[:2]
    if w < 2000:
        r = 2000/w
        d = (int(w*r), int(h*r))
        imagedata = cv2.resize(imagedata, d)
    bbs = textdetect.detect_box(imagepath, imagedata)
    bbs.sort(key=lambda x: x[1])

    angle, grouped = group_bounding_boxes(bbs, 0.7*imagedata.shape[1])
    j = 0
    NIK_str, Nama_str = '', ''
    for i in range(len(grouped)):
        if i != 2 and i != 3:
            continue
        denoise = True
        if detect_mode == 'O':
            model = 'NIK' if i == 2 else 'etkp'
        else:
            model = 'ind'
            denoise = None
        line_text = []
        for k in grouped[i]:
            j += 1
            db = j
            if i == 3 and len(grouped[i])-1 == grouped[i].index(k):
                db = 'test'
            _, img = crop_and_rotate_opencv(imagedata, bbs[k], filename, debug_tag = db, segments=None, preset_angle=angle, denoise=denoise)
            result = tesseract.run_tesseract({imagepath: img}, lang=model)
            key = f'{filename[:-4]}.tsv'
            ocr_str = result[key][:-2]
            if len(grouped[i])-1 == grouped[i].index(k):
                if i == 2:
                    ocr_str = ocr_str[-16:]
                    NIK_str = ocr_str
                else:
                    while len(ocr_str) > 0 and ocr_str[0] not in string.ascii_letters:
                        ocr_str = ocr_str[1:]
                    Nama_str = ocr_str
            line_text.append(ocr_str)
            # if i == 3:
            #     result = tesseract.run_tesseract({imagepath: img}, lang='ind')
            #     key = f'{filename[:-4]}.tsv'
            #     ocr_str = result[key][:-2]
            #     line_text.append(ocr_str)
        joined_text = '<>'.join(line_text)
        print(f' {detect_mode} line {i}: >{joined_text}<')
        # DEBUG = False

    return {'NIK': NIK_str, 'Nama': Nama_str}

    print(5*' ' + 16*'-')
    if DEBUG:
        for bb in bbs:
            cv2bb = []
            for i in range(len(bb)//2):
                cv2bb.append([bb[2*i], bb[2*i+1]])
            pts = np.array(cv2bb, np.int32)
            pts = pts.reshape((-1,1,2))
            cv2.polylines(imagedata, [pts], True, (255,0,0))
            j += 1
        cv2.imwrite(f'bb- {filename}', imagedata)
    # if DEBUG:
    #     font = cv2.FONT_HERSHEY_PLAIN
    #     imagedata = cv2.putText(imagedata, ocr_str1, (int(bbs[2][0]), int(bbs[2][1])), font, 4, (0,0,255), 2)
    #     imagedata = cv2.putText(imagedata, ocr_str2, (int(bbs[3][0]), int(bbs[3][1])), font, 4, (0,0,255), 2)
    #     cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- ocr.jpg'), imagedata)

def remove_color(img, filename):
    white = [255, 255, 255]
    fixed_l = np.sqrt(3)
    [h,w] = img.shape[:2]
    gray = np.array([1,1,1], dtype=np.uint8)
    for i in range(h):
        for j in range(w):
            v = img[i,j]
            d = np.linalg.norm(v, ord=4)
            if d > 150:
                img[i,j] = white
    if True or DEBUG:
        cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- remove_color.jpg'), img)
    return img

def remove_color_bis(img, filename):
    n, sqrt3 = np.sqrt(2)/2, np.sqrt(3)
    rotation_matrix_z = np.array(((n, -n, 0), (n, n, 0), (0, 0, 1)))
    t = np.arctan(np.sqrt(2))
    c, s = np.cos(t), np.sin(t)
    rotation_matrix_x = np.array(((0, c, -s), (0, s, c), (1, 0, 0)))
    rotation_matrix = rotation_matrix_x.dot(rotation_matrix_z)
    white = [255, 255, 255]
    white = [200, 200, 200]
    [h,w] = img.shape[:2]
    r, a = 0.5, 2
    for i in range(h):
        for j in range(w):
            v = img[i,j]
            [x, y, z] = rotation_matrix.dot(v)/255
            # if x*x + y*y - (z-sqrt3)*(z-sqrt3) > 0:
            if x*x/r + y*y/r - (z-a)*(z-a) > 0:
                img[i,j] = white
    if True or DEBUG:
        cv2.imwrite(join(DEBUG_PATH, f'{filename[:-4]}- remove_color.jpg'), img)
    return img

def draw_bounding_boxes(filename, path):
    print(f'\n{filename}')
    imagepath = join(path, filename)
    imagedata = cv2.cvtColor(np.array(Image.open(imagepath).convert('RGB')), cv2.COLOR_RGB2BGR)
    bbs = textdetect.detect_box(imagepath, imagedata)
    bbs.sort(key=lambda x: x[1])
    bb = bbs[0]
    angle = compute_bb_angle(np.array([[bb[2*i], bb[2*i+1]] for i in range(len(bb)//2)]))
    j = 0
    for bb in bbs:
        cv2bb = []
        angle, _ = crop_and_rotate_opencv(imagedata, bb, filename, debug_tag=j, preset_angle=angle)
        for i in range(len(bb)//2):
            cv2bb.append([bb[2*i], bb[2*i+1]])
        pts = np.array(cv2bb, np.int32)
        pts = pts.reshape((-1,1,2))
        cv2.polylines(imagedata, [pts], True, (255,0,0))
        j += 1
    cv2.imwrite(f'bb-{filename}', imagedata)

def getNIKNamaFromTextfile(filename, lepath):
    res = {}
    with open(join(lepath+'textfiles', filename[:-4]+'.txt')) as f:
        lines = f.readlines()
        tmp = lines[2][lines[2].index(': ')+2:]
        res['NIK'] = ""
        for c in tmp:
            if c in string.digits:
                res['NIK'] += c
        tmp = lines[3][lines[3].index(': ')+2:]
        while len(tmp) > 0 and tmp[-1] not in string.ascii_uppercase:
            tmp = tmp[:-1] 
        res['Nama'] = tmp
    print(f'Expected: >{res["NIK"]}<   >{res["Nama"]}<')
    return res

if __name__ == "__main__":
    textdetect = TextdetectClass( None, model_path='/mnt/textdetect/checkpoints_mlt/ctpn_50000.ckpt', dumpfile=False, debug=False,
                            gunicorn=False, show_individual_box=False, gpu_memory_fraction=0.08 )
    tesseract = PyTesseractClass(None, './models', '/mnt/ocris2/tessexe/tessdata/configs', dumpfile=False, debug=False)
    denoisingbg = DenoisingBg('./', dumpfile=False, debug=False)


    # files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    # for filename  in files:
    #     do_ocr_passport(filename, mypath)

    # d = np.linalg.norm(np.cross([1,1,1],[1,0,0]))
    # print(f'd: {d}')
    # print(f'sqrt2: {np.sqrt(2)}')
    # exit(0)

    # DEBUG = True
    # mypath = './idimages'
    mypath = './idimages2'
    # mypath = './passportimages'
    files1 = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    files = ['NOT_CONTACTED3275046209890014_KTP.png', 'NOT_CONTACTED3174055206900002_KTP.png', 'images7371142008840009_KTP.png', 'images3174106505950005_KTP.png', 'ATO3322181601920002_KTP.png', 'images2171110310909004_KTP.png', 'images6471030608920003_KTP.png', 'images3173061501880004_KTP.png', 'images3671072902920002_KTP.png', 'images2171102509750001_KTP.png', 'images2171026901880002_KTP.png', 'DEFAULT23210084806920021_KTP.png', 'HARD_REJ3276116503850003_KTP.png', 'NOT_CONTACTED3174040701880004_KTP.png', 'DEFAULT23172016509790006_KTP.png', 'images1271181702860001_KTP.png', 'HARD_REJ1671144901000005_KTP.png', 'DEFAULT27371023012930002_KTP.png', 'images3674021904840002_KTP.png', 'images1509035204960001_KTP.png', 'images1571075106970001_KTP.png', 'images5171042512870004_KTP.png', 'ATO3275010603930023_KTP.png', 'DEFAULT23174056312770004_KTP.png', 'images1771022306900011_KTP.png', 'DEFAULT21701112407890001_KTP.png', 'images3173024103940003_KTP.png', 'HARD_REJ3174041306001003_KTP.png', 'DEFAULT23276055910790012_KTP.png', 'DEFAULT27371103003860001_KTP.png', 'DEFAULT23515041610910003_KTP.png', 'images3174074307780003_KTP.png', 'DEFAULT23273204306880002_KTP.png', 'images3173066506800013_KTP.png', 'HARD_REJ1571021102916041_KTP.png', 'images1271217008920001_KTP.png', 'images3328090402850004_KTP.png', 'NOT_CONTACTED3578102708940007_KTP.png', 'DEFAULT23171075107860003_KTP.png', 'NOT_CONTACTED3276046403920005_KTP.png', 'images3578304601710004_KTP.png', 'DEFAULT23175072401800003_KTP.png', 'DEFAULT23671091501860001_KTP.png', 'images3273020108910010_KTP.png', 'images1671090812900010_KTP.png', 'images3275046703960008_KTP.png', 'HARD_REJ3175050905870002_KTP.png', 'HARD_REJ7271010905980003_KTP.png', 'images1671015904930001_KTP.png', 'DEFAULT23175010410880003_KTP.png', 'images3275046901770005_KTP.png', 'images3216064401910008_KTP.png', 'HARD_REJ3173060703700008_KTP.png', 'images1471025607840001_KTP.png', 'ATO1271215608958004_KTP.png', 'DEFAULT27171044408900001_KTP.png', 'HARD_REJ3171030305910008_KTP.png', 'HARD_REJ1803106603860010_KTP.png', 'DEFAULT23671104511920004_KTP.png', 'images3175046410860002_KTP.png', 'images6472042203950003_KTP.png', 'NOT_CONTACTED3578136509840004_KTP.png', 'DEFAULT23174056003930001_KTP.png', 'DEFAULT23578014612850002_KTP.png', 'HARD_REJ3173071307770004_KTP.png', 'DEFAULT23175060303930016_KTP.png', 'ATO3275026209900022_KTP.png', 'HARD_REJ1872010307910004_KTP.png', 'images3374102802870002_KTP.png', 'images1809051609920002_KTP.png', 'images3515111712880001_KTP.png', 'images1371022308950010_KTP.png', 'HARD_REJ5104033011730003_KTP.png', 'images3276095411910001_KTP.png', 'ATO1271210804940002_KTP.png', 'HARD_REJ1505011201000001_KTP.png', 'DEFAULT23319032010860004_KTP.png', 'images3672015002960001_KTP.png', 'images1206041406940002_KTP.png', 'DEFAULT23204446104970001_KTP.png', 'HARD_REJ6471042006850006_KTP.png', 'images3174021805870003_KTP.png', 'DEFAULT21671105011880009_KTP.png', 'images1610050303900002_KTP.png', 'images1208290812930003_KTP.png', 'images5108060910880003_KTP.png', 'images3174091404660002_KTP.png', 'ATO3525106705890001_KTP.png', 'DEFAULT23175061708840049_KTP.png', 'HARD_REJ3507270406950001_KTP.png', 'HARD_REJ5204085108910001_KTP.png', 'images3515070711960005_KTP.png', 'DEFAULT23173072304880002_KTP.png', 'images3174051006961002_KTP.png', 'DEFAULT22171060903870002_KTP.png', 'images1204162804970003_KTP.png', 'images3171044903920004_KTP.png', 'images3172027101950003_KTP.png', 'images3173025711890006_KTP.png', 'images3174064209930002_KTP.png', 'DEFAULT27371032010930001_KTP.png', 'HARD_REJ3201021908930009_KTP.png', 'images3172022510950004_KTP.png', 'images3671085603730005_KTP.png', 'images1702190305860001_KTP.png', 'NOT_CONTACTED3603026411890004_KTP.png', 'HARD_REJ3273304612910007_KTP.png', 'DEFAULT21702221708940001_KTP.png', 'NOT_CONTACTED1205131404900001_KTP.png', 'images3173023110880001_KTP.png', 'NOT_CONTACTED5203101002810001_KTP.png', 'HARD_REJ5108066703950008_KTP.png', 'images3172012906750003_KTP.png', 'images5171016508710005_KTP.png', 'images3515176012930002_KTP.png', 'images1809016403950003_KTP.png', 'NOT_CONTACTED7326012503850002_KTP.png', 'images3175056105890004_KTP.png', 'images3275010812930023_KTP.png', 'DEFAULT23272011409960920_KTP.png', 'images7314075409790002_KTP.png', 'NOT_CONTACTED3173072512920003_KTP.png', 'ATO1810011001950005_KTP.png', 'DEFAULT23173076712810007_KTP.png', 'NOT_CONTACTED3175020101760009_KTP.png', 'images3271041109970009_KTP.png', 'DEFAULT21202014701900001_KTP.png', 'images1901010807800001_KTP.png', 'ATO3271066702970017_KTP.png', 'HARD_REJ1671060505940008_KTP.png', 'DEFAULT21607103007880004_KTP.png', 'images1272052701910003_KTP.png', 'images3603174511670002_KTP.png', 'images2172040309900004_KTP.png', 'images6472062209900001_KTP.png', 'NOT_CONTACTED3174062609810011_KTP.png', 'NOT_CONTACTED3276056503780011_KTP.png', 'images1271205708880002_KTP.png', 'images3174096803891001_KTP.png', 'images3173055711780002_KTP.png']
    faulty_files = ['images7371142008840009_KTP.png', 'images3174106505950005_KTP.png', 'ATO3322181601920002_KTP.png', 'images6471030608920003_KTP.png', 'images3173061501880004_KTP.png', 'images2171102509750001_KTP.png', 'images2171026901880002_KTP.png', 'DEFAULT23210084806920021_KTP.png', 'HARD_REJ3276116503850003_KTP.png', 'NOT_CONTACTED3174040701880004_KTP.png', 'DEFAULT23172016509790006_KTP.png', 'images1271181702860001_KTP.png', 'HARD_REJ1671144901000005_KTP.png', 'images3674021904840002_KTP.png', 'images1571075106970001_KTP.png', 'ATO3275010603930023_KTP.png', 'DEFAULT23174056312770004_KTP.png', 'images1771022306900011_KTP.png', 'HARD_REJ3174041306001003_KTP.png', 'DEFAULT23276055910790012_KTP.png', 'DEFAULT27371103003860001_KTP.png', 'DEFAULT23515041610910003_KTP.png', 'DEFAULT23273204306880002_KTP.png', 'HARD_REJ1571021102916041_KTP.png', 'images1271217008920001_KTP.png', 'images3328090402850004_KTP.png', 'NOT_CONTACTED3578102708940007_KTP.png', 'DEFAULT23171075107860003_KTP.png', 'NOT_CONTACTED3276046403920005_KTP.png', 'DEFAULT23175072401800003_KTP.png', 'DEFAULT23671091501860001_KTP.png', 'images3273020108910010_KTP.png', 'images1671090812900010_KTP.png', 'images3275046703960008_KTP.png', 'HARD_REJ3175050905870002_KTP.png', 'HARD_REJ7271010905980003_KTP.png', 'DEFAULT23175010410880003_KTP.png', 'images3275046901770005_KTP.png', 'HARD_REJ3173060703700008_KTP.png', 'images1471025607840001_KTP.png', 'ATO1271215608958004_KTP.png', 'HARD_REJ3171030305910008_KTP.png', 'HARD_REJ1803106603860010_KTP.png', 'DEFAULT23671104511920004_KTP.png', 'images3175046410860002_KTP.png', 'images6472042203950003_KTP.png', 'NOT_CONTACTED3578136509840004_KTP.png', 'DEFAULT23174056003930001_KTP.png', 'DEFAULT23578014612850002_KTP.png', 'ATO3275026209900022_KTP.png', 'HARD_REJ1872010307910004_KTP.png', 'images1809051609920002_KTP.png', 'images3515111712880001_KTP.png', 'images1371022308950010_KTP.png', 'HARD_REJ5104033011730003_KTP.png', 'images3276095411910001_KTP.png', 'ATO1271210804940002_KTP.png', 'HARD_REJ1505011201000001_KTP.png', 'DEFAULT23319032010860004_KTP.png', 'DEFAULT23204446104970001_KTP.png', 'HARD_REJ6471042006850006_KTP.png', 'images3174021805870003_KTP.png', 'images1610050303900002_KTP.png', 'images1208290812930003_KTP.png', 'images5108060910880003_KTP.png', 'images3174091404660002_KTP.png', 'ATO3525106705890001_KTP.png', 'DEFAULT23175061708840049_KTP.png', 'HARD_REJ5204085108910001_KTP.png', 'images3515070711960005_KTP.png', 'DEFAULT23173072304880002_KTP.png', 'images3174051006961002_KTP.png', 'DEFAULT22171060903870002_KTP.png', 'images3174064209930002_KTP.png', 'DEFAULT27371032010930001_KTP.png', 'HARD_REJ3201021908930009_KTP.png', 'images3172022510950004_KTP.png', 'images3671085603730005_KTP.png', 'images1702190305860001_KTP.png', 'DEFAULT21702221708940001_KTP.png', 'NOT_CONTACTED1205131404900001_KTP.png', 'images3173023110880001_KTP.png', 'NOT_CONTACTED5203101002810001_KTP.png', 'HARD_REJ5108066703950008_KTP.png', 'images5171016508710005_KTP.png', 'images1809016403950003_KTP.png', 'images3175056105890004_KTP.png', 'images3275010812930023_KTP.png', 'DEFAULT23272011409960920_KTP.png', 'images7314075409790002_KTP.png', 'NOT_CONTACTED3173072512920003_KTP.png', 'ATO1810011001950005_KTP.png', 'DEFAULT23173076712810007_KTP.png', 'NOT_CONTACTED3175020101760009_KTP.png', 'DEFAULT21202014701900001_KTP.png', 'images1901010807800001_KTP.png', 'ATO3271066702970017_KTP.png', 'images1272052701910003_KTP.png', 'images2172040309900004_KTP.png', 'NOT_CONTACTED3276056503780011_KTP.png', 'images3174096803891001_KTP.png', 'images3173055711780002_KTP.png']
    print(f'{len(files1)} {len(files)} {len(faulty_files)}')
    faulty = []

    # i, NIK, Nama = 0,0,0
    # for filename  in files:
    #     i += 1
    #     # do_ocr_passport(filename, mypath)
    #     # draw_bounding_boxes(filename, mypath)
    #     resO = do_ocr_id(filename, mypath, detect_mode='O')
    #     ground_truth = getNIKNamaFromTextfile(filename, mypath)

    #     nik_O, nama_O = jf.jaro_winkler_similarity(ground_truth["NIK"], resO["NIK"]), jf.jaro_winkler_similarity(ground_truth["Nama"], resO["Nama"])

    #     if nik_O > 0.9:
    #         NIK += 1
    #     if nama_O > 0.9:
    #         Nama += 1
    #     print(f'NIK: {(NIK*100/i):.2f}%, Nama: {(Nama*100/i):.2f}%')


    for s in range(45, 65):
        for c in range(-5,6):
            i = 0
            SPREAD, INTERCEPT = s, c
            nik_eq, nik_ObH, nik_HbO, nama_eq, nama_ObH, nama_HbO = 0,0,0,0,0,0
            for filename  in files:
                i += 1
                # do_ocr_passport(filename, mypath)
                # draw_bounding_boxes(filename, mypath)
                resO = do_ocr_id(filename, mypath, detect_mode='O')
                resH = do_ocr_id(filename, mypath, detect_mode='H')
                ground_truth = getNIKNamaFromTextfile(filename, mypath)

                nik_O, nik_H, nama_O, nama_H = jf.jaro_winkler_similarity(ground_truth["NIK"], resO["NIK"]), jf.jaro_winkler_similarity(ground_truth["NIK"], resH["NIK"]), jf.jaro_winkler_similarity(ground_truth["Nama"], resO["Nama"]), jf.jaro_winkler_similarity(ground_truth["Nama"], resH["Nama"])
                print(f'Similarities: {nik_O:.3} {nik_H:.3} {nama_O:.3} {nama_H:.3}')

                if nik_O == nik_H:
                    nik_eq += 1
                elif nik_O > nik_H:
                    nik_ObH += 1
                else:
                    nik_HbO += 1
                if nama_O == nama_H:
                    nama_eq += 1
                elif nama_O > nama_H:
                    nama_ObH += 1
                else:
                    nama_HbO += 1

                print(f'NIK:  O beats H: {(nik_ObH*100/i):.2f} H beats O: {(nik_HbO*100/i):.2f} O == H: {(nik_eq*100/i):.2f}')            
                print(f'Nama: O beats H: {(nama_ObH*100/i):.2f} H beats O: {(nama_HbO*100/i):.2f} O == H: {(nama_eq*100/i):.2f}')
            with open('results.txt', 'a+') as f:
                f.write(f'std spread: {s} and offset: {c}\n')
                f.write(f'NIK:  O beats H: {(nik_ObH*100/i):.2f} H beats O: {(nik_HbO*100/i):.2f} O == H: {(nik_eq*100/i):.2f}\n')
                f.write(f'Nama: O beats H: {(nama_ObH*100/i):.2f} H beats O: {(nama_HbO*100/i):.2f} O == H: {(nama_eq*100/i):.2f}\n')
                f.write('\n')

        # if jf.jaro_winkler_similarity(ground_truth['NIK'], NIK_str) < 0.9 or jf.jaro_winkler_similarity(ground_truth['Nama'], Nama_str) < 0.9:
        #     return filename
        # # if ground_truth['NIK'] != NIK_str or ground_truth['Nama'] != Nama_str:
        # #     return filename
        # if res is not None:
        #     faulty.append(res)
        # print(f'{len(faulty)} out of {i} are faulty')
        # break
