#!/usr/bin/env python
# -*- coding:utf-8 mode:python -*-
#
import os
import sys
import shutil
import cv2
import numpy as np
import argparse
import time

from common.function import is_image_in_memory, get_image_info, get_file_list
from common.config import SUFFIX_MASK, SUPPORTED_IMG_TYPES

class DBGException(Exception): pass
class DBGPathError(DBGException): pass


class DenoisingBg(object):

	def __init__(self, output_dir, dumpfile=True, debug=False):
		self.output_dir = output_dir
		self.lower      = 0
		self.upper      = 255
		self.debug      = debug
		self.dumpfile   = dumpfile
		self.infilemem  = None

		# error checking
		if self.dumpfile and not os.path.isdir(self.output_dir):
			raise(DBGPathError('{}: output folder not found !!!'.format(self.output_dir)))


	def run_denoising(self, inputimage):
		
		try:
			self.infilemem = is_image_in_memory(inputimage)
		except Exception as e:
			print('Exception: {}'.format(e))
			raise
		
		if self.debug:
			print('Input image:     {}'.format(os.path.basename(get_image_info(inputimage)[0])))

		#self.upper = self.compute_image_threshold(inputimage)
		imresult = {}
		# imresult.update(self.copy_original_image(inputimage))
		
		#imresult.update(self.create_grayscale_image(inputimage))   # gray generate poor result
		#imresult.update(self.create_mask_image(inputimage))
		imresult.update(self.create_cleaned_image(inputimage))
		
		if self.debug:
			for imgname, imgself in imresult.items():
				print('-> Output image: {}'.format(os.path.basename(imgname)))

		return imresult


	def copy_original_image(self, inputimage):
		#print('copy_original_image')
		imgname, imgself = get_image_info(inputimage)
		if self.infilemem:      # memory file
			if self.dumpfile:
				cv2.imwrite(os.path.join(self.output_dir, imgname), imgself)
			return {imgname : imgself}
		else:                   # disk file
			if self.dumpfile:
				shutil.copy2(inputimage, self.output_dir)
			return {imgname : cv2.imread(inputimage, cv2.IMREAD_UNCHANGED)}


	def create_cleaned_image(self,inputimage):
		
		imgname, imgself = get_image_info(inputimage)
		
		if self.infilemem:
			image = imgself.copy()
		else:
			image = cv2.imread(inputimage)
				
		image = self.image_normalize(image)		
		#image = self.isblurred(image)		
		image = self.compute_RGB_shift(image)		
		#ratio_initial, magnitude_spectrum, fft_plot = self.fourier_transform(image)		
		image_masked, image_thresholded = self.background(image)		
		# ratio_precleaned = self.fourier_transform(image_masked)
		ratio_precleaned = 0
		
		image_added, image_added_blur, image_white = self.add_points(image_masked)
		
		image_precleaned = self.remove_points(image_added)
		image_precleaned = self.image_normalize(image_precleaned)
		
		image_added_blur = self.image_normalize(image_added_blur)
		image_cleaning = self.image_cleaning(image_added_blur)
		
		if (ratio_precleaned < 2.0):
			image = image_precleaned
		else:
			image = image_cleaning

		fresname = '{1}{0}{2}'.format(SUFFIX_MASK, *os.path.splitext(os.path.basename(imgname)))
		if self.dumpfile:
			cv2.imwrite(os.path.join(self.output_dir, fresname), image)

		return {fresname : image}
	

	def isblurred(self,inputimage):
		image = inputimage
		n_row, n_col, _ = image.shape
		image_to_analyse = image[10:-10,10:int(0.7*n_col),:]
		gray_image = cv2.cvtColor(image_to_analyse, cv2.COLOR_BGR2GRAY)
		blur_parameter = cv2.Laplacian(gray_image, cv2.CV_64F).var()
		
		if blur_parameter <= 250:
			kernel = np.array([[-1,-1,-1],[-1, 9,-1],[-1,-1,-1]])
			image = cv2.filter2D(image, -1, kernel)
		
		return image


	def isfinger(self, inputimage):
		image = inputimage
		n_row, n_col, _ = image.shape
		imgray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
		ret,thresh = cv2.threshold(imgray,127,255,0)
		print("ret: ", ret)
		print("tresh: ", thresh)
		image, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

		return


	def fourier_transform(self,inputimage):
		image = inputimage

		image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
		n_row, n_col = image.shape

		dft = cv2.dft(np.float32(image),flags = cv2.DFT_COMPLEX_OUTPUT)
		dft_shift = np.fft.fftshift(dft)
		#print("dft: ", dft.shape, dft)
		#print("dft_shift: ", dft_shift.shape, dft_shift)

		magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))
		def smooth(x,window_len=11,window='hanning'):
			if x.ndim != 1:
				raise(ValueError, "smooth only accepts 1 dimension arrays.")

			if x.size < window_len:
				raise(ValueError, "Input vector needs to be bigger than window size.") 

			if window_len<3:
				return x

			if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
				raise(ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")

			s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
			#print(len(s))

			if window == 'flat': #moving average
				w=np.ones(window_len,'d')
			else:
				w=eval('np.'+window+'(window_len)')

			y=np.convolve(w/w.sum(),s,mode='valid')

			return y
		
		image_transposed = np.transpose(image)
		image_transposed_fft = np.fft.fft(image_transposed)/n_row
		image_transposed_fft = np.absolute(image_transposed_fft)
		image_transposed_fft = np.sum(image_transposed_fft,axis = 0)
		image_transposed_fft = image_transposed_fft[range(0,n_row/2)]
		#print(image_transposed_fft)
		freq = np.array(range(0,n_row/2))

		image_transposed_fft_range_100_to_200 = image_transposed_fft[range(100,200)]
		peak_value = np.max(image_transposed_fft_range_100_to_200)
		peak_position = np.argmax(image_transposed_fft_range_100_to_200)
		pedestal_value = np.min(image_transposed_fft_range_100_to_200[range(0,peak_position+1)])
		pedestal_position = np.argmin(image_transposed_fft_range_100_to_200[range(0,peak_position+1)])
		peak_pedestal_ratio = peak_value / pedestal_value

		#print("peak_position: ", peak_position)
		#print("peak_value: ", peak_value)
		#print("pedestal_value: ", pedestal_value)
		#print("pedestal_position: ", pedestal_position)
		#print("peak_pedestal_ratio: ", peak_pedestal_ratio)
		#image_transposed_fft_smooth = smooth(image_transposed_fft, 21)
		#image_transposed_fft_smooth = image_transposed_fft_smooth[range(9,n_row/2+8)]

		#freq = np.array(range(0,n_row/2))

		#figure = plt.figure(figsize=(10, 10))
		#plt.plot(freq, image_transposed_fft, 'r-', freq, image_transposed_fft_smooth, 'b-')
		#plt.xlabel('freq (Hz)')
		#plt.ylabel('|Y(freq)|')
		#plt.show()

		return peak_pedestal_ratio


	def background(self, inputimage):
		image = inputimage
		image_MedianBlur = cv2.medianBlur(image,75)
		image_BilateralFilter = cv2.bilateralFilter(image,55,75,75)

		image_difference = 255 - (np.array(image_MedianBlur, dtype = "int") - np.array(image_BilateralFilter,dtype="int"))
		image_difference[image_difference > 255] = 255
		image_difference[image_difference < 0] = 0
		image_difference = np.array(image_difference, dtype = "uint8")

		image_gray = cv2.cvtColor(image_difference, cv2.COLOR_BGR2GRAY)
		image_thresholded = cv2.adaptiveThreshold(image_gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,25,2)

		image_masked = image
		image_masked[:,:,0] = 255 - np.array((255 - image_thresholded)/255, dtype = "int") * (255 - image_difference[:,:,0])
		image_masked[:,:,1] = 255 - np.array((255 - image_thresholded)/255, dtype = "int") * (255 - image_difference[:,:,1])
		image_masked[:,:,2] = 255 - np.array((255 - image_thresholded)/255, dtype = "int") * (255 - image_difference[:,:,2])
		image_masked = np.array(image_masked, dtype = "uint8")

		return image_masked, image_thresholded


	def image_cleaning(self, inputimage):
		image = inputimage
		
		image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		blur = cv2.GaussianBlur(image_gray,(5,5),0)
		ret,thr = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

		image_cleaned = image
		image_cleaned[:,:,0] = (255-thr)/255 * (255 - image[:,:,0])
		image_cleaned[:,:,1] = (255-thr)/255 * (255 - image[:,:,1])
		image_cleaned[:,:,2] = (255-thr)/255 * (255 - image[:,:,2])
		image_cleaned = 255 - image_cleaned

		return image_cleaned


	def remove_points(self,inputimage):
		image = inputimage
		image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		n_row, n_col = image_gray.shape

		image_white = (image_gray != 255) * 1

		image_with_edges = np.zeros((n_row+2, n_col+2), dtype = 'int')
		image_with_edges[1:n_row+1, 1:n_col+1] = image_white
		remove = np.ones((n_row, n_col), dtype = 'int')

		while remove.sum() > 0:
			remove_1 = ((image_with_edges[0:n_row, 1:n_col+1] + image_with_edges[2:n_row+2, 1:n_col+1] + image_with_edges[1:n_row+1, 0:n_col] + image_with_edges[1:n_row+1, 2:n_col+2]) < 2) * image_white
			remove_2 = (image_with_edges[0:n_row, 1:n_col+1] + image_with_edges[2:n_row+2, 1:n_col+1] == 2) * (image_with_edges[1:n_row+1, 0:n_col] + image_with_edges[1:n_row+1, 2:n_col+2] == 0) * image_white
			remove_3 = (image_with_edges[0:n_row, 1:n_col+1] + image_with_edges[2:n_row+2, 1:n_col+1] == 0) * (image_with_edges[1:n_row+1, 0:n_col] + image_with_edges[1:n_row+1, 2:n_col+2] == 2) * image_white
			remove = ((remove_1 + remove_2  + remove_3) > 0) * 1
			image_white = ((image_white - remove) > 0) * 1
			image_with_edges[1:n_row+1, 1:n_col+1] = image_white

		image_cleaned = image
		image_cleaned[:,:,0] = 255 - (image_white) * (255 - image[:,:,0])
		image_cleaned[:,:,1] = 255 - (image_white) * (255 - image[:,:,1])
		image_cleaned[:,:,2] = 255 - (image_white) * (255 - image[:,:,2])

		return image_cleaned


	def add_points(self,inputimage):
		image = inputimage

		image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		n_row, n_col = image_gray.shape
		image_white = (image_gray == 255) * 1

		image_with_edges = np.zeros((n_row+2, n_col+2), dtype = 'int')
		image_with_edges[1:n_row+1, 1:n_col+1] = image_white
		add = np.ones((n_row, n_col), dtype = 'int')

		add_1 = ((image_with_edges[0:n_row, 1:n_col+1] + image_with_edges[2:n_row+2, 1:n_col+1] + \
			image_with_edges[0:n_row, 2:n_col+2] + image_with_edges[1:n_row+1, 2:n_col+2] + image_with_edges[2:n_row+2, 2:n_col+2])==0) * \
			image_white

		add_2 = ((image_with_edges[0:n_row, 1:n_col+1] + image_with_edges[2:n_row+2, 1:n_col+1] + \
			image_with_edges[0:n_row, 0:n_col+0] + image_with_edges[1:n_row+1, 0:n_col+0] + image_with_edges[2:n_row+2, 0:n_col+0])==0) * \
			image_white

		add_3 = ((image_with_edges[1:n_row+1, 0:n_col] + image_with_edges[1:n_row+1, 2:n_col+2] + \
			image_with_edges[2:n_row+2, 0:n_col] + image_with_edges[2:n_row+2, 1:n_col+1] + image_with_edges[2:n_row+2, 2:n_col+2])==0) * \
			image_white
		
		add_4 = ((image_with_edges[1:n_row+1, 0:n_col] + image_with_edges[1:n_row+1, 2:n_col+2] + \
			image_with_edges[0:n_row+0, 0:n_col] + image_with_edges[0:n_row+0, 1:n_col+1] + image_with_edges[0:n_row+0, 2:n_col+2])==0) * \
			image_white

		add = ((add_1 + add_2 + add_3 + add_4) > 0) * 1
		image_white = ((image_white - add) > 0) * 1
		
		blur = cv2.GaussianBlur(image,(5,5),0)

		image_output_1 = image
		image_output_1[:,:,0] = (1-add) * image[:,:,0] + add * blur[:,:,0]
		image_output_1[:,:,1] = (1-add) * image[:,:,1] + add * blur[:,:,1]
		image_output_1[:,:,2] = (1-add) * image[:,:,2] + add * blur[:,:,2]
		image_output_1 = np.array(image_output_1, dtype = "uint8")

		image_output_2 = image
		image_output_2[:,:,0] = image_white * image[:,:,0] + (1 - image_white) * blur[:,:,0]
		image_output_2[:,:,1] = image_white * image[:,:,1] + (1 - image_white) * blur[:,:,1]
		image_output_2[:,:,2] = image_white * image[:,:,2] + (1 - image_white) * blur[:,:,2]
		image_output_2 = np.array(image_output_2, dtype = "uint8")

		return image_output_1, image_output_2, add*255


	def color_equalization(self, inputimage):
		image = inputimage
		
		image_YCrCb = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)
		image_YCrCb[:,:,0] = cv2.equalizeHist(image_YCrCb[:,:,0])
		image_output = cv2.cvtColor(image_YCrCb, cv2.COLOR_YCrCb2BGR)
		
		return image_output


	def image_normalize_towhite(self,inputimage,detected_boxes):
		
		image_normalize = np.array(inputimage, dtype = "float32")
		image_no_boxes = inputimage.copy()
		n_row, n_col, _ = inputimage.shape

		for box in detected_boxes:
			row_1 = max(box[1]-5,0)
			row_2 = min(box[3]+5,n_row-1)
			col_1 = max(box[0]-5,0)
			col_2 = min(box[2]+5,n_col-1)
			image_no_boxes[row_1:row_2,col_1:col_2,:] = 255
			
		def white_point(image):
			image_1d_0 = image[:,:,0].flatten()
			image_1d_0 = image_1d_0[image_1d_0 < 255]
			image_1d_1 = image[:,:,1].flatten()
			image_1d_1 = image_1d_1[image_1d_1 < 255]
			image_1d_2 = image[:,:,2].flatten()
			image_1d_2 = image_1d_2[image_1d_2 < 255]

			wp_0 = min(image_1d_0[np.argsort(image_1d_0) ][int(0.05 * image_1d_0.shape[0]) - 1:])
			wp_1 = min(image_1d_1[np.argsort(image_1d_1) ][int(0.05 * image_1d_1.shape[0]) - 1:])
			wp_2 = min(image_1d_2[np.argsort(image_1d_2) ][int(0.05 * image_1d_2.shape[0]) - 1:])
			return wp_0, wp_1, wp_2

		def black_point(image):
			image_1d_0 = image[:,:,0].flatten()
			image_1d_1 = image[:,:,1].flatten()
			image_1d_2 = image[:,:,2].flatten()
			bp_0 = max(image_1d_0[np.argsort(image_1d_0) ][:int(0.05 * image_1d_0.shape[0]) + 1])
			bp_1 = max(image_1d_1[np.argsort(image_1d_1) ][:int(0.05 * image_1d_1.shape[0]) + 1])
			bp_2 = max(image_1d_2[np.argsort(image_1d_2) ][:int(0.05 * image_1d_2.shape[0]) + 1])
			return bp_0, bp_1, bp_2


		wp_0_0, wp_1_0, wp_2_0 = white_point(image_no_boxes[10:-10,10:int(0.7*n_col),:])
		bp_0_0, bp_1_0, bp_2_0 = black_point(image_normalize[10:-10,10:int(0.7*n_col),:])
		
		wp_0_1, wp_1_1, wp_2_1 = white_point(image_no_boxes[10:-10,150:int(0.7*n_col),:])
		bp_0_1, bp_1_1, bp_2_1 = black_point(image_normalize[10:-10,150:int(0.7*n_col),:])
		
		wp_0_2, wp_1_2, wp_2_2 = white_point(image_no_boxes[100:-10,10:int(0.7*n_col),:])
		bp_0_2, bp_1_2, bp_2_2 = black_point(image_normalize[100:-10,10:int(0.7*n_col),:])
		
		wp_0_3, wp_1_3, wp_2_3 = white_point(image_no_boxes[10:-100,10:int(0.7*n_col),:])
		bp_0_3, bp_1_3, bp_2_3 = black_point(image_normalize[10:-100,10:int(0.7*n_col),:])

		whitepoint_0 = max(wp_0_0,wp_0_1,wp_0_2,wp_0_3)
		whitepoint_1 = max(wp_1_0,wp_1_1,wp_1_2,wp_1_3)
		whitepoint_2 = max(wp_2_0,wp_2_1,wp_2_2,wp_2_3)

		blackpoint_0 = max(bp_0_0,bp_0_1,bp_0_2,bp_0_3)
		blackpoint_1 = max(bp_1_0,bp_1_1,bp_1_2,bp_1_3)
		blackpoint_2 = max(bp_2_0,bp_2_1,bp_2_2,bp_2_3)

		#whitepoint_0 = wp_0_0
		#whitepoint_1 = wp_1_0
		#whitepoint_2 = wp_2_0

		#blackpoint_0 = bp_0_0
		#blackpoint_1 = bp_1_0
		#blackpoint_2 = bp_2_0

		print("whitepoint_0, blackpoint_0: ", whitepoint_0, blackpoint_0)
		print("whitepoint_1, blackpoint_1: ", whitepoint_1, blackpoint_1)
		print("whitepoint_2, blackpoint_2: ", whitepoint_2, blackpoint_2)	
		
		image_normalize[:,:,0] = (np.array(image_normalize[:,:,0],dtype='float32') - blackpoint_0) / (whitepoint_0 - blackpoint_0) * 255
		image_normalize[:,:,1] = (np.array(image_normalize[:,:,1],dtype='float32') - blackpoint_1) / (whitepoint_1 - blackpoint_1) * 255
		image_normalize[:,:,2] = (np.array(image_normalize[:,:,2],dtype='float32') - blackpoint_2) / (whitepoint_2 - blackpoint_2) * 255
		
		image_normalize[image_normalize < 0.] = 0
		image_normalize[image_normalize > 255.] = 255
		image_normalize = np.array(image_normalize, dtype = "uint8")
		
		#cv2.normalize(image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_64F)
		
		return image_normalize


	def image_normalize(self,inputimage):
		image_normalize = inputimage
		image_normalize[:,:,0] = 255 * (np.array(image_normalize[:,:,0],dtype='float32') - image_normalize[:,:,0].min()) / (image_normalize[:,:,0].max() - image_normalize[:,:,0].min())
		image_normalize[:,:,1] = 255 * (np.array(image_normalize[:,:,1],dtype='float32') - image_normalize[:,:,1].min()) / (image_normalize[:,:,1].max() - image_normalize[:,:,1].min())
		image_normalize[:,:,2] = 255 * (np.array(image_normalize[:,:,2],dtype='float32') - image_normalize[:,:,2].min()) / (image_normalize[:,:,2].max() - image_normalize[:,:,2].min())
		
		#cv2.normalize(image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_64F)
		
		return image_normalize


	def compute_RGB_shift(self, inputimage):
		#print('compute_RGB_shift')
		image = inputimage

		def square_diff(image_1, image_2):
			n_row, n_col = image_1.shape
			cost_function = (((np.array(image_1,dtype='float32') - np.array(image_2,dtype='float32'))/255)**2).sum() / n_row / n_col
			return cost_function

		n_row, n_col, n_colors = image.shape
		image_Blue = image[:,:,0]
		image_Red = image[:,:,1]
		image_Green = image[:,:,2]

		max_cell = 6
		from itertools import combinations_with_replacement
		comb = list(combinations_with_replacement(range(-5,6), 2))
		square_diff_list_BR = map(lambda x: square_diff(image_Blue[max_cell:(n_row - max_cell), max_cell:(n_col - max_cell)],image_Red[(max_cell + x[0]):(n_row - max_cell + x[0]), (max_cell + x[1]):(n_col - max_cell + x[1])]), comb)
		index_min_BR = np.argmin(square_diff_list_BR)
		optim_shift_BR = comb[index_min_BR]
		
		square_diff_list_BG = map(lambda x: square_diff(image_Blue[max_cell:(n_row - max_cell), max_cell:(n_col - max_cell)],image_Green[(max_cell + x[0]):(n_row - max_cell + x[0]), (max_cell + x[1]):(n_col - max_cell + x[1])]), comb)
		index_min_BG = np.argmin(square_diff_list_BG)
		optim_shift_BG = comb[index_min_BG]

		imgshifted = image
		M_red_blue = np.float32([[1,0,-optim_shift_BR[1]],[0,1,-optim_shift_BR[0]]])
		M_green_blue = np.float32([[1,0,-optim_shift_BG[1]],[0,1,-optim_shift_BG[0]]])
		imgshifted[:,:,1] = cv2.warpAffine(image[:,:,1],M_red_blue,(n_col,n_row))
		imgshifted[:,:,2] = cv2.warpAffine(image[:,:,2],M_green_blue,(n_col,n_row))

		return(imgshifted)


	def resolution_down(self,inputimage):
		image = inputimage
		image_low_res = cv2.pyrDown(image)

		return(image_low_res)


	def resolution_up(self,inputimage):
		image = inputimage
		image_high_res = cv2.pyrUp(image)

		return(image_high_res)


	def image_cleaning(self, inputimage):
		#print('image_cleaning')
		image = inputimage
		img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		# Otsu's thresholding
		ret2,th2 = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
		# Otsu's thresholding after Gaussian filtering
		blur = cv2.GaussianBlur(img,(5,5),0)
		ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

		image1 = image
		image1[:,:,0] = (255-th3)/255 * (255 - image[:,:,0])
		image1[:,:,1] = (255-th3)/255 * (255 - image[:,:,1])
		image1[:,:,2] = (255-th3)/255 * (255 - image[:,:,2])
		image_processed = 255 - image1

		return(image_processed)


	def remove_small_patterns(self, inputimage):
		#print('remove_small_patterns')
		image = inputimage
		image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		n_row, n_col = image_gray.shape
		image_white = (image_gray != 255) * 1

		image_with_edges = np.zeros((n_row+2, n_col+2), dtype = 'int')
		image_with_edges[1:n_row+1, 1:n_col+1] = image_white
		remove = np.ones((n_row, n_col), dtype = 'int')

		while remove.sum() > 0:
			remove_1 = ((image_with_edges[0:n_row, 1:n_col+1] + image_with_edges[2:n_row+2, 1:n_col+1] + image_with_edges[1:n_row+1, 0:n_col] + image_with_edges[1:n_row+1, 2:n_col+2]) < 2) * image_white
			remove_2 = (image_with_edges[0:n_row, 1:n_col+1] + image_with_edges[2:n_row+2, 1:n_col+1] == 2) * (image_with_edges[1:n_row+1, 0:n_col] + image_with_edges[1:n_row+1, 2:n_col+2] == 0) * image_white
			remove_3 = (image_with_edges[0:n_row, 1:n_col+1] + image_with_edges[2:n_row+2, 1:n_col+1] == 0) * (image_with_edges[1:n_row+1, 0:n_col] + image_with_edges[1:n_row+1, 2:n_col+2] == 2) * image_white
			remove = ((remove_1 + remove_2  + remove_3) > 0) * 1
			image_white = ((image_white - remove) > 0) * 1
			image_with_edges[1:n_row+1, 1:n_col+1] = image_white

		image_cleaned = image
		image_cleaned[:,:,0] = 255 - (image_white) * (255 - image[:,:,0])
		image_cleaned[:,:,1] = 255 - (image_white) * (255 - image[:,:,1])
		image_cleaned[:,:,2] = 255 - (image_white) * (255 - image[:,:,2])

		return image_cleaned



if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', '--in',       dest='infolder',  help='input folder',  required=True)
	parser.add_argument('-o', '--out',      dest='outfolder', help='output folder', required=True)
	parser.add_argument('-d', '--debug',    dest='debug',     help='debug option',  required=False, action='store_true', default=False)
	parser.add_argument('-m', '--memfile',  dest='memfile',   help='memory file',   required=False, action='store_true', default=False)

	args      = parser.parse_args()
	infolder  = os.path.normpath(args.infolder)  # remove trailing os.sep if any
	outfolder = os.path.normpath(args.outfolder) # remove trailing os.sep if any
	debug     = args.debug
	memfile   = args.memfile

	# TODO: remove existing files
	for d in (outfolder, ):
		try:
			os.mkdir(d)
			os.chmod(d, 0o777)
		except OSError:
			if not os.path.isdir(d):
				print('Error creating folder output: {}'.format(d))
				sys.exit(1)

	print('\n')
	print('Input  folder: {}'.format(infolder))
	print('Output folder: {}'.format(outfolder))
	print('debug        : {}'.format('True' if debug else 'False'))
	print('memfile      : {}'.format('True' if memfile else 'False'))
	print('\n')

	input_images = []
	if memfile:                 # read memory file input
		for img in get_file_list(infolder, SUPPORTED_IMG_TYPES):
			input_images.append({ os.path.basename(img) : cv2.imread(img, cv2.IMREAD_UNCHANGED)})
	else:                       # read disk file input
		input_images = get_file_list(infolder, SUPPORTED_IMG_TYPES)

	start_time = time.time()
	try:
		db = DenoisingBg(outfolder, debug=debug)
	except Exception as e:
		print('::EXCEPTION: {}'.format(e))
		sys.exit(1)  
	
	for img in input_images:
		try:
			db.run_denoising(img)
		except Exception as e:
			print('::EXCEPTION: {}'.format(e))
			sys.exit(1)

	print('Done in {} seconds'.format(time.time() - start_time))
	
